let html = ``;
const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];
data.forEach(function({name, image, price_small, price_large}) {
    html = `${html}<article class="pizzaThumbnail">
        <a href="${image}">
           <img src="${image}"/>
            <section>
                <h4>${name}</h4>
               <ul>
                   <li>Prix petit format : ${price_small} €</li>
                   <li>Prix grand format : ${price_large} €</li>
               </ul>
           </section>
        </a>
    </article>`;
    console.log(html);
    document.querySelector('.pageContent').innerHTML = html;
})